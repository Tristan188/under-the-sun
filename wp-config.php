<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'underthesun' );

/** MySQL database username */
define( 'DB_USER', 'server1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'OO3!E-C}P4LD' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'de~D;pE~7*B@S*&-wtQ*4:]#6!XasAHM#&KNSll=G7-AmG%@}.Fy)O8Nk(li}j5t' );
define( 'SECURE_AUTH_KEY',  '4Owc=J8/= Yr;Q>.~)oVu(2f5Gfs9y6As@!8[RSg4ksX=)X***qc:7jE?]!e-rgf' );
define( 'LOGGED_IN_KEY',    'm]B&tD@9<{6O.-bg] d|`C^2Y5#o(|s!eEAW02/]szQ_k|p.5F7.V4d&MKBDZiqP' );
define( 'NONCE_KEY',        'Q#g)YB4H8u S?c^ E@gbsQ$/. :Dfa<S(rHnhe+B9g7I.{&R)-=-D},g9q=rgD4:' );
define( 'AUTH_SALT',        'MF]y)x0;a0k!/Tor9qJQ#UUeO<tDf@:V/*ICR_tiQ97w?qeG~8f)0T!j,i=7KzMV' );
define( 'SECURE_AUTH_SALT', 'jV2[pLVq2rGE(g{p$4MOO_S;*jqP?:yxol!]^$sT.n{t {Dop)XZM$=0`Zx38;d@' );
define( 'LOGGED_IN_SALT',   'wU]b<g`Eg^UZ?+*J=%wX,6h@ZEm-!_55P#`*;D_F,:t`]Y8aw_eaATVz)}z]gz,Y' );
define( 'NONCE_SALT',       'C|)8K#Jk!f+@!lg223IH4uCf-Vmh<8!UQ6)sexTOfwk0r%7![9HOU%a9 [`DF?p1' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
